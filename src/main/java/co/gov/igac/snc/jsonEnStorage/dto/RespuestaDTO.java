package co.gov.igac.snc.jsonEnStorage.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RespuestaDTO {

	private String rutaDestino;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy/MM/dd HH:mm")
	private Date fechaHoraEnvio;
	private List<ArchivoDTO> archivos;
}
