package co.gov.igac.snc.jsonEnStorage.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PeticionDTO {

	private String rutaOrigen;
	private String rutaDestino;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy/MM/dd HH:mm")
	private Date fechaEnvio;
	private String origen;
	private String observacion;
}
