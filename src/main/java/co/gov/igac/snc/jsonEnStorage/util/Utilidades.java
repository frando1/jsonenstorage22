package co.gov.igac.snc.jsonEnStorage.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionLecturaDeArchivo;
import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionArchivoNoExiste;

public class Utilidades {

	public static void leerProperties(Properties properties) 
			throws ExcepcionArchivoNoExiste, ExcepcionLecturaDeArchivo {
		try {
			properties.load(new FileReader("D:\\properties\\properties.properties"));
		} catch (FileNotFoundException ex) {
			throw new ExcepcionArchivoNoExiste(ex.getMessage());
		} catch (IOException ex) {
			throw new ExcepcionLecturaDeArchivo(ex.getMessage());
		}
	}
	
	public static ResponseEntity<?> consumirApi(Map<String, String> peticion, String urlApi) {
		TcpClient tcpClient = TcpClient
	            .create()
	            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000)
	            .doOnConnected(connection -> {
	                connection.addHandlerLast(new ReadTimeoutHandler(10000, TimeUnit.MILLISECONDS));
	                connection.addHandlerLast(new WriteTimeoutHandler(10000, TimeUnit.MILLISECONDS));
	            });
		WebClient webClient = WebClient.builder()
				.clientConnector(new ReactorClientHttpConnector(HttpClient.from(tcpClient)))
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
		
		ResponseEntity<?> respuesta = webClient.post()
		        .uri(urlApi)
		        .body(Mono.just(peticion),Map.class)
		        .retrieve()
		        .toEntity(String.class)
		        .block();
		
		return respuesta;
	}
	
	public static ResponseEntity<?> consumirApi(String peticion, String urlApi) {
		TcpClient tcpClient = TcpClient
	            .create()
	            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
	            .doOnConnected(connection -> {
	                connection.addHandlerLast(new ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS));
	                connection.addHandlerLast(new WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS));
	            });
		WebClient webClient = WebClient.builder()
				.clientConnector(new ReactorClientHttpConnector(HttpClient.from(tcpClient)))
                .baseUrl(urlApi)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
		
		ResponseEntity<?> respuesta = webClient.post()
		        .uri("/subirArchivo")
		        .body(Mono.just(peticion),String.class)
		        .retrieve()
		        //.onStatus(HttpStatus::is5xxServerError, clientResponse -> Mono.empty())
		        .toEntity(String.class)
		        .block();
		
		return respuesta;
	}
}
