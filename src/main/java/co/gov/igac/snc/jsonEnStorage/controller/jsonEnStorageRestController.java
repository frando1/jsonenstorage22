package co.gov.igac.snc.jsonEnStorage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import co.gov.igac.snc.jsonEnStorage.dto.PeticionDTO;
import co.gov.igac.snc.jsonEnStorage.dto.RespuestaDTO;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionArchivoNoExiste;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionDeNegocio;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionLecturaDeArchivo;
import co.gov.igac.snc.jsonEnStorage.service.IJsonEnStorageService;

@RestController
public class jsonEnStorageRestController {
	
	@Autowired
	IJsonEnStorageService service;

	@PostMapping("/guardarArchivoDatalake")
	public ResponseEntity<?> guardarArchivoDatalake(@RequestBody PeticionDTO peticion) 
			throws ExcepcionArchivoNoExiste, ExcepcionLecturaDeArchivo, ExcepcionDeNegocio{
		
		System.out.println(peticion);
		
		RespuestaDTO respuesta = service.subirArchivo(peticion);
		return ResponseEntity.ok(respuesta);		
	}
}
