package co.gov.igac.snc.jsonEnStorage.exception;

import java.io.FileNotFoundException;

import org.springframework.http.HttpStatus;
import lombok.Getter;


public class ExcepcionArchivoNoExiste extends FileNotFoundException {
	
	private static final long serialVersionUID = 1L;
	
	@Getter
	private HttpStatus estado;

	public ExcepcionArchivoNoExiste() {
		super();
	}	

	public ExcepcionArchivoNoExiste(String mensaje) {
		super(mensaje);
	}	

	public ExcepcionArchivoNoExiste(String mensaje, HttpStatus estado) {
		super(mensaje);
		this.estado = estado;
	}	
	
}
