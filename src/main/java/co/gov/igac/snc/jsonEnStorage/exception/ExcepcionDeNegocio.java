package co.gov.igac.snc.jsonEnStorage.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class ExcepcionDeNegocio extends Exception{

	private static final long serialVersionUID = 1L;

	@Getter
	private HttpStatus estado;
	@Getter
	private String titulo;
	
	public ExcepcionDeNegocio(String mensaje, String titulo, HttpStatus estado) {
		super(mensaje);
		this.estado = estado;
		this.titulo = titulo;
	}
}
