package co.gov.igac.snc.jsonEnStorage.service.impl;


import static co.gov.igac.snc.jsonEnStorage.util.Utilidades.leerProperties;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

import com.azure.core.http.rest.PagedIterable;
import com.azure.identity.ClientSecretCredential;
import com.azure.identity.ClientSecretCredentialBuilder;
import com.azure.storage.file.datalake.DataLakeDirectoryClient;
import com.azure.storage.file.datalake.DataLakeFileClient;
import com.azure.storage.file.datalake.DataLakeFileSystemClient;
import com.azure.storage.file.datalake.DataLakeServiceClient;
import com.azure.storage.file.datalake.DataLakeServiceClientBuilder;
import com.azure.storage.file.datalake.models.ListPathsOptions;
import com.azure.storage.file.datalake.models.PathItem;

import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionLecturaDeArchivo;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionArchivoNoExiste;

public class AzureStorage {

	private String accountName;
	private String clientId;
	private String clientSecret;
	private String tenantId;
	private String contenedor;

	private DataLakeServiceClient dataLakeSC;

	public AzureStorage() throws ExcepcionArchivoNoExiste, ExcepcionLecturaDeArchivo {
		this.properties();
		this.getDataLakeServiceClient();
	}
	

	
	private void getDataLakeServiceClient(){
	
	    String endpoint = "https://" + accountName + ".dfs.core.windows.net";
	    
	    ClientSecretCredential clientSecretCredential = new ClientSecretCredentialBuilder()
		    .clientId(clientId)
		    .clientSecret(clientSecret)
		    .tenantId(tenantId)
		    .build();    
	       
	    DataLakeServiceClientBuilder builder = new DataLakeServiceClientBuilder();	   
	    
	    this.dataLakeSC = builder.credential(clientSecretCredential).endpoint(endpoint).buildClient();
	}

	public void subirArchivo(String rutaArchivo, String rutaStorage, String nombreArchivo)
			throws ExcepcionArchivoNoExiste {
		
		DataLakeFileSystemClient fileSystemClient = dataLakeSC.getFileSystemClient(contenedor);

		DataLakeDirectoryClient directoryClient = fileSystemClient.getDirectoryClient(rutaStorage);

		DataLakeFileClient fileClient = directoryClient.createFile(nombreArchivo);

		File file = new File(rutaArchivo);

		// InputStream targetStream = new FileInputStream(file);
		InputStream targetStream = null;
		try {
			targetStream = new BufferedInputStream(new FileInputStream(file));
		} catch (FileNotFoundException ex) {
			throw new ExcepcionArchivoNoExiste(ex.getMessage());
		}

		long fileSize = file.length();

		fileClient.append(targetStream, 0, fileSize);

		fileClient.flush(fileSize);
	}
	
	public void listarArchivosEnDirectorio(String rutaStorage, String nombreArchivo){
	    
		DataLakeFileSystemClient fileSystemClient = dataLakeSC.getFileSystemClient(contenedor);
		
	    ListPathsOptions options = new ListPathsOptions();
	    options.setPath(rutaStorage);
	 
	    PagedIterable<PathItem> pagedIterable = 
	    fileSystemClient.listPaths(options, null);

	    Iterator<PathItem> iterator = pagedIterable.iterator();

	   
	    PathItem item = iterator.next();

	    while (item != null)
	    {
	        System.out.println(item.getName());


	        if (!iterator.hasNext())
	        {
	            break;
	        }
	        
	        item = iterator.next();
	    }

	}
	
	public boolean subirArchivoGrande(String rutaArchivo, String rutaStorage, String nombreArchivo)
			throws ExcepcionArchivoNoExiste {

		DataLakeFileSystemClient fileSystemClient = dataLakeSC.getFileSystemClient(contenedor);

		DataLakeDirectoryClient directoryClient = fileSystemClient.getDirectoryClient(rutaStorage);

		DataLakeFileClient fileClient = directoryClient.getFileClient(nombreArchivo);

		fileClient.uploadFromFile(rutaArchivo);
		
		return fileClient.exists();

	}
	
	public boolean existeArchivo(String rutaStorage, String nombreArchivo) {
		
		DataLakeFileSystemClient fileSystemClient = dataLakeSC.getFileSystemClient(contenedor);

		DataLakeDirectoryClient directoryClient = fileSystemClient.getDirectoryClient(rutaStorage);

		DataLakeFileClient fileClient = directoryClient.getFileClient(nombreArchivo);
		
		return fileClient.exists();
	}
	
	public boolean existeDirectorio(String rutaStorage) {
		
		DataLakeFileSystemClient fileSystemClient = dataLakeSC.getFileSystemClient(contenedor);

		DataLakeDirectoryClient directoryClient = fileSystemClient.getDirectoryClient(rutaStorage);

		return directoryClient.exists();
	}
	
	public boolean crearDirectorio(String ruta){
		
	    DataLakeFileSystemClient fileSystemClient = dataLakeSC.getFileSystemClient(contenedor);

	    return fileSystemClient.createDirectory(ruta).exists();

	    //return directoryClient.createSubdirectory(nuevoDirectorio);
	}
	

	
	private void properties() throws ExcepcionArchivoNoExiste, ExcepcionLecturaDeArchivo{		
		Properties properties = new Properties();
		leerProperties(properties);
		this.accountName = properties.getProperty("accountName");
		this.clientId = properties.getProperty("clientId");
		this.clientSecret = properties.getProperty("clientSecret");
		this.tenantId = properties.getProperty("tenantId");
		this.contenedor = properties.getProperty("contenedor");
	}
}
