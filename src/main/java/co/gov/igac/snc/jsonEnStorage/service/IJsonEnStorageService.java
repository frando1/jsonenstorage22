package co.gov.igac.snc.jsonEnStorage.service;

import co.gov.igac.snc.jsonEnStorage.dto.PeticionDTO;
import co.gov.igac.snc.jsonEnStorage.dto.RespuestaDTO;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionLecturaDeArchivo;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionArchivoNoExiste;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionDeNegocio;

public interface IJsonEnStorageService {

	public RespuestaDTO subirArchivo(PeticionDTO peticion) 
			throws ExcepcionArchivoNoExiste, ExcepcionLecturaDeArchivo, ExcepcionDeNegocio;
}
