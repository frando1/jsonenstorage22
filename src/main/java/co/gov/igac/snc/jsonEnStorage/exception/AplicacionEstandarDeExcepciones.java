package co.gov.igac.snc.jsonEnStorage.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import co.gov.igac.snc.jsonEnStorage.dto.EstandarDeExcepcionesDTO;

/**
 * 
 * @author jdrodriguezo
 * @version 1.0
 */
@RestControllerAdvice
public class AplicacionEstandarDeExcepciones {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<EstandarDeExcepcionesDTO> handleNoContentException(Exception ex){
		ex.printStackTrace();
		EstandarDeExcepcionesDTO respuesta = new EstandarDeExcepcionesDTO.ExceptionBuilder()
				.tipo("Exception")
				.titulo("Error interno")
				.codigo("E400")
				.estado(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR))
				.detalle(ex.getMessage())
				.instancia("/guardarArchivoDatalake")
				.builder();
		return new ResponseEntity<EstandarDeExcepcionesDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<EstandarDeExcepcionesDTO> handleNoContentException(HttpMessageNotReadableException ex){
		ex.printStackTrace();
		EstandarDeExcepcionesDTO respuesta = new EstandarDeExcepcionesDTO.ExceptionBuilder()
				.tipo("HttpMessageNotReadableException")
				.titulo("Error de lectura de dato JSON")
				.codigo("E500")
				.estado(String.valueOf(HttpStatus.BAD_REQUEST))
				.detalle(ex.getMessage())
				.instancia("/guardarArchivoDatalake")
				.builder();
		return new ResponseEntity<EstandarDeExcepcionesDTO>(respuesta, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<EstandarDeExcepcionesDTO> handleNoContentException(IllegalArgumentException ex){
		ex.printStackTrace();
		EstandarDeExcepcionesDTO respuesta = new EstandarDeExcepcionesDTO.ExceptionBuilder()
				.tipo("IllegalArgumentException")
				.titulo("Error de Archivo")
				.codigo("E600")
				.estado(String.valueOf(HttpStatus.BAD_REQUEST))
				.detalle(ex.getMessage())
				.instancia("/guardarArchivoDatalake")
				.builder();
		return new ResponseEntity<EstandarDeExcepcionesDTO>(respuesta, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ExcepcionArchivoNoExiste.class)
	public ResponseEntity<EstandarDeExcepcionesDTO> handleNoContentException(ExcepcionArchivoNoExiste ex){
		ex.printStackTrace();
		HttpStatus estado = ex.getEstado() == null ? HttpStatus.NOT_IMPLEMENTED : ex.getEstado();
		EstandarDeExcepcionesDTO respuesta = new EstandarDeExcepcionesDTO.ExceptionBuilder()
				.tipo("FileNotFoundException")
				.titulo("Error de archivo no encontrado")
				.codigo("E100")
				.estado(String.valueOf(estado.value()))
				.detalle(ex.getMessage())
				.instancia("/guardarArchivoDatalake")
				.builder();
		return new ResponseEntity<EstandarDeExcepcionesDTO>(respuesta, estado);
	}
	
	@ExceptionHandler(ExcepcionLecturaDeArchivo.class)
	public ResponseEntity<EstandarDeExcepcionesDTO> handleNoContentException(ExcepcionLecturaDeArchivo ex){
		ex.printStackTrace();
		HttpStatus estado = ex.getEstado() == null ? HttpStatus.NOT_IMPLEMENTED : ex.getEstado();
		EstandarDeExcepcionesDTO respuesta = new EstandarDeExcepcionesDTO.ExceptionBuilder()
				.tipo("IOException")
				.titulo("Error de lectura de archivo")
				.codigo("E200")
				.estado(String.valueOf(estado.value()))
				.detalle(ex.getMessage())
				.instancia("/guardarArchivoDatalake")
				.builder();
		return new ResponseEntity<EstandarDeExcepcionesDTO>(respuesta, estado);
	}
	
	@ExceptionHandler(ExcepcionDeNegocio.class)
	public ResponseEntity<EstandarDeExcepcionesDTO> handleNoContentException(ExcepcionDeNegocio ex){
		ex.printStackTrace();
		HttpStatus estado = ex.getEstado() == null ? HttpStatus.NOT_IMPLEMENTED : ex.getEstado();
		EstandarDeExcepcionesDTO respuesta = new EstandarDeExcepcionesDTO.ExceptionBuilder()
				.tipo("Excepcion de negocio")
				.titulo(ex.getTitulo())
				.codigo("E300")
				.estado(String.valueOf(estado.value()))
				.detalle(ex.getMessage())
				.instancia("/guardarArchivoDatalake")
				.builder();
		return new ResponseEntity<EstandarDeExcepcionesDTO>(respuesta, estado);
	}
}
