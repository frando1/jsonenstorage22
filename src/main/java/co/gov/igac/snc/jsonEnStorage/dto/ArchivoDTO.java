package co.gov.igac.snc.jsonEnStorage.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArchivoDTO {

	private String nombreArchivo;
	private String codigoStatus;
}
