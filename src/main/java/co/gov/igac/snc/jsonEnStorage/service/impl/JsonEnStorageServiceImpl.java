package co.gov.igac.snc.jsonEnStorage.service.impl;

import java.io.File;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import co.gov.igac.snc.jsonEnStorage.dto.ArchivoDTO;
import co.gov.igac.snc.jsonEnStorage.dto.PeticionDTO;
import co.gov.igac.snc.jsonEnStorage.dto.RespuestaDTO;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionLecturaDeArchivo;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionArchivoNoExiste;
import co.gov.igac.snc.jsonEnStorage.exception.ExcepcionDeNegocio;
import co.gov.igac.snc.jsonEnStorage.service.IJsonEnStorageService;

@Service
public class JsonEnStorageServiceImpl implements IJsonEnStorageService{
	
	AzureStorage storage;

	@Override
	public RespuestaDTO subirArchivo(PeticionDTO peticion) 
			throws ExcepcionArchivoNoExiste, ExcepcionLecturaDeArchivo, ExcepcionDeNegocio {
		
		storage = new AzureStorage();
		
		List<String> mensajes = validarPeticion(peticion);
		if(mensajes.size() > 0) {
			throw new ExcepcionDeNegocio(
					"Solicitud incorrecta: " + mensajes.toString(),
					"Error en la petición",
					HttpStatus.BAD_REQUEST);
		}
		
		Map<String, String> rutaArchivo = dividirRuta(peticion.getRutaDestino());
		String directorioStorage = rutaArchivo.get("rutaStorage")+"/"+rutaArchivo.get("nombreDirectorio");
		boolean existeDirectorio = storage.existeDirectorio(directorioStorage);
		if(!existeDirectorio) {
			existeDirectorio = storage.crearDirectorio(directorioStorage);
		}
		boolean jsonEnStorage = false;
		Date fechaHora = null;
		if(existeDirectorio) {
			fechaHora = Timestamp.valueOf(LocalDateTime.now());
			jsonEnStorage = storage.subirArchivoGrande(peticion.getRutaOrigen(), directorioStorage, rutaArchivo.get("nombreArchivo"));
		}
		if(!jsonEnStorage) {
			throw new ExcepcionDeNegocio(
					"No se pudo subir el archivo de manera correcta", 
					"Error al subir archivo", 
					HttpStatus.BAD_REQUEST);
		}
		RespuestaDTO respuesta = new RespuestaDTO();
		respuesta.setRutaDestino(directorioStorage+"/"+rutaArchivo.get("nombreArchivo"));
		respuesta.setFechaHoraEnvio(fechaHora);		
		respuesta.setArchivos(new ArrayList<>());
		respuesta.getArchivos().add(new ArchivoDTO(peticion.getOrigen(),String.valueOf(HttpStatus.OK.toString())));
		return respuesta;
	}
	
	private List<String> validarPeticion(PeticionDTO peticion) {
		List<String> mensaje = new ArrayList<>();
		if(peticion == null) {
			mensaje.add("La petición no puede estar nula ó vacía");
		}
		if(peticion.getOrigen() == null || peticion.getOrigen().isBlank()) {
			mensaje.add("El campo 'origen' de la petición no puede ir nulo ó vacío");
		}
		if(peticion.getRutaOrigen() == null || peticion.getRutaOrigen().isBlank()) {
			mensaje.add("El campo 'rutaOrigen' de la petición no puede ir nulo ó vacío");
		} else {
			if(!peticion.getRutaOrigen().matches("^.*\\.json$")) {
				mensaje.add("La ruta '" + peticion.getRutaOrigen() +"' no contiene el nombre del archivo .json");
			}
			if(!new File(peticion.getRutaOrigen()).exists()) {
				mensaje.add("El archivo '" + peticion.getRutaOrigen() +"' no existe");
			}
		}
		if(peticion.getRutaDestino() == null || peticion.getRutaDestino().isBlank()) {
			mensaje.add("El campo 'rutaDestino' de la petición no puede ir nulo ó vacío");
		} else {
			Map<String, String> rutaArchivo = dividirRuta(peticion.getRutaDestino());
			if(storage.existeArchivo(rutaArchivo.get("rutaStorage") +"/"+ rutaArchivo.get("nombreDirectorio"), rutaArchivo.get("nombreArchivo"))) {
				mensaje.add("Ya existe un archivo con este nombre '" + rutaArchivo.get("nombreArchivo") + "'");
			}
		}
		if(!peticion.getOrigen().toUpperCase().equals("IGAC") && !peticion.getOrigen().toUpperCase().equals("SNR") ) {
			mensaje.add("El valor '" + peticion.getOrigen() +"' del campo 'origen' no corresponde a un dato valido");
		}		
		
		return mensaje;
	}
	
	private Map<String, String> dividirRuta(String storage) {
		String[] ruta = storage.split("/");
		String rutaStorage = "";
		for(int i = 0; i < ruta.length-1; i++) {
			rutaStorage+= (i > 0 ? "/" : "") + ruta[i] ;
		}
		String nombreArchivo = ruta[ruta.length-1];
		String[] nombreA = nombreArchivo.split("_");

		String nombreArchivoJson = "";
		String nombreDirectorio = nombreA[1];
		for(int i = 1; i < nombreA.length; i++) {
			nombreArchivoJson+= (i > 1 ? "_" : "") + nombreA[i] ;
		}
		Map<String, String> rutaArchivo = new HashMap<String, String>();
		rutaArchivo.put("nombreArchivo", nombreArchivoJson);
		rutaArchivo.put("nombreDirectorio", nombreDirectorio);
		rutaArchivo.put("rutaStorage", rutaStorage);
		return rutaArchivo;
	}
	
}
